/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
google.load("visualization", "1", {packages: ["corechart"]});

//google.setOnLoadCallback(drawStuff);


google.load('visualization', '1.0', {'packages':['corechart'], 'callback': incializeChart1});

function incializeChart1() {
   // var urljson = "http://dados.fortaleza.ce.gov.br/dataset/83659092-a920-47c5-8b23-ae5cef33d1ce/resource/6fc1d58a-0f75-4b9a-b44c-285f4ba6dbbe/download/benstombados.json";
    $.getJSON(urljson, function (data) {
        
         var  datatable = new google.visualization.DataTable();
                    datatable.addRows(3);
                    datatable.addColumn('string', 'Esfera');
                    datatable.addColumn('number', 'tombados');
                    datatable.addColumn('number', 'naotombados');
                    
         var qtdeTombado =[];
         var qtdeNaoTombado = [];
         var esferas = ['Federal', 'Estadual', 'Municipal'];
                    
         $.each(data.features, function (i, feature) {     
          
                    //data.addColumn('number', 'Value:', 'value');
                    // data.addColumn({type: 'string', role: 'tooltip'});
            var situacao = obtemSituacaoTombamento(feature.properties.DESCRICAO);
     
                    
            for (var i = 0; i < 3; i++) {
                qtdeTombado[i]=0;
                qtdeNaoTombado[i]=0;
            }

            $.each(data.features, function(i, feature) {
                if(situacao.v) {
                    if(feature.properties.TTOMB === 'Federal') {
                        qtdeTombado[0] = qtdeTombado[0] + 1;
                    } else if(feature.properties.TTOMB === 'Estadual') {
                        qtdeTombado[1] = qtdeTombado[0] + 1;
                    } else {
                        qtdeTombado[2] = qtdeTombado[0] + 1;
                    }                    
                } else {
                     if(feature.properties.TTOMB === 'Federal') {
                        qtdeNaoTombado[0] = qtdeTombado[0] + 1;
                    } else if(feature.properties.TTOMB === 'Estadual') {
                        qtdeNaoTombado[1] = qtdeTombado[0] + 1;
                    } else {
                        qtdeNaoTombado[2] = qtdeTombado[0] + 1;
                    } 
                }
            });

        });
        
        
        for (var i = 0; i < 3; i++) {
            datatable.addRows([[esferas[i], 
                    qtdeTombado[i], qtdeNaoTombado[i]
            ]]);
        }
        var options = {
          width: 500,
          chart: {
            title: 'Tombamentos na Cidade de Fortaleza ',
            subtitle: 'Número de tombamentos  e processos abertos por esfera'
          },
          series: {
            0: { axis: 'tombados' }, // Bind series 0 to an axis named 'distance'.
            1: { axis: 'naotombados' } // Bind series 1 to an axis named 'brightness'.
          },
          axes: {
            y: {
              tombados: {label: 'Tombados'}, // Left y-axis.
              naotombados: {side: 'right', label: 'Em processo de Tombamento'} // Right y-axis.
            }
          }
        };
        
        
        
      var chart = new google.visualization.BarChart(document.getElementById('chart1'));
      chart.draw(datatable, options);
   });
}