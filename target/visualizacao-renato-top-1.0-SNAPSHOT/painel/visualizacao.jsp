<%-- 
    Document   : visualizacao
    Created on : 17/10/2016, 21:17:07
    Author     : helis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Visualizacao</title>
        <link href="../resources/css/painel.css" rel="stylesheet">
        <style>
            .infoBox { background-color: #FFF; width: 300px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border: 2px solid #3fa7d8; border-radius: 3px; margin-top: 10px }
            .infoBox p { padding: 0 15px }
            .infoBox:before { border-left: 10px solid transparent; border-right: 10px solid transparent; border-bottom: 10px solid #3fa7d8; top: -10px; content: ""; height: 0; position: absolute; width: 0; left: 138px }
        </style>
    </head>
    <body>
    <div id="header" role="banner">
        <div>
            <div id="logo">
                <a id="portal-logo" href="#">
                    <span id="portal-title-1">IFBA</span>
                    <h1 id="portal-title" class="luongo">Tombamentos na cidade Fortaleza</h1>
                    <span id="portal-description">Uma visualizacão simplificada</span>
                </a>
            </div>
        </div>
    </div>
    <div id="main" role="main">
        <div id="portal-content"
             <div class="row">
                <div id="mapa" style="height:500px; width: 600px;" ></div>
                 <div id="legend">
                    Legenda:
                         <div class="display-flex"><div class="legend-box" style="background: #3FD0C3"></div> Tombado</div>
                         <div class="display-flex"><div class="legend-box" style="background: #E1851C;"></div> Em Processo de tombamento</div>
                 </div>                    
              </div>                
            <div class="row">
                <div id="chart1" style="width: 900px; height: 500px;"></div>
            </div>
        </div>
    </div>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDDvAeRRvHhAKfAZzAh8I6s6lxXVO9wz4s"></script>
<script src="https://raw.githubusercontent.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
<script src="https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/src/markerclusterer.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="../resources/js/process.js"></script>
<script src="../resources/js/mapprocess.js"></script>
<script src="../resources/js/chart1.js"></script>
</body>
</html>

