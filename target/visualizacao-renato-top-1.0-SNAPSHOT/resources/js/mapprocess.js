var filtro = "ALL";
var map;

initialize();
carregarPontos();

var idInfoBoxAberto;
var infoBox = [];

function abrirInfoBox(id, marker) {
    if (typeof (idInfoBoxAberto) == 'number' && typeof (infoBox[idInfoBoxAberto]) == 'object') {
        infoBox[idInfoBoxAberto].close();
    }

    infoBox[id].open(map, marker);
    idInfoBoxAberto = id;
}

function carregarPontos() {

    var govApi = "http://dados.fortaleza.ce.gov.br/dataset/83659092-a920-47c5-8b23-ae5cef33d1ce/resource/6fc1d58a-0f75-4b9a-b44c-285f4ba6dbbe/download/benstombados.json";
    $.getJSON(govApi, function (data) {

        var markers=[];
        //var latlngbounds = new google.maps.LatLngBounds();

        $.each(data.features, function (i, feature) {

            var LONGITUDE = parseFloat(feature.geometry.coordinates[0]);
            var LATITUDE = parseFloat(feature.geometry.coordinates[1]);
            var situacao = obtemSituacaoTombamento(feature.properties.DESCRICAO);
            if (filtro === "ALL"
                    || (filtro === "TOMB" && situacao.v)
                    || (filtro === "NOTOMB" && !situacao.v)
                    ) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(LATITUDE, LONGITUDE),
                    title: feature.properties.NOME,
                    icon: obtemImagemIcon(situacao)
                });

                var myOptions = {
                    content: "<p>"+ feature.properties.NOME+"</p> \
                        <p  style='font-size:12px;font-style:italic'>" + situacao.f + " - "
                            + feature.properties.TTOMB +"</p>"
                            ,
                    pixelOffset: new google.maps.Size(-150, 0)
                };

                infoBox[parseInt(feature.properties.GID)] = new InfoBox(myOptions);
                infoBox[parseInt(feature.properties.GID)].marker = marker;

                infoBox[parseInt(feature.properties.GID)].listener = google.maps.event.addListener(marker, 'click', function (e) {
                    abrirInfoBox(parseInt(feature.properties.GID), marker);
                });
                
                markers.push(marker);
            }
            
           var markerCluster = new MarkerClusterer(map, markers);
           //map.fitBounds(latlngbounds);
        });

    });
}

function initialize() {
    var latlng = new google.maps.LatLng(-3.7183943,-38.5433948);

    var options = {
        
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        ZoomControl: true,
        zoomControlOptions: {
           style: google.maps.ZoomControlStyle.SMALL,
        },
        streetViewControl: false,
    };
    
    map = new google.maps.Map(document.getElementById("mapa"), options);
    
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(
    document.getElementById('legend'));
}

function obtemImagemIcon(situacao) {

    if (situacao.v !== 1) {
        return "https://openclipart.org/image/40px/svg_to_png/215472/marker_orange.png";
    }
    return  "https://openclipart.org/image/40px/svg_to_png/215471/1425664103.png";
}
