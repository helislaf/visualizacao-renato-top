/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function obtemSituacaoTombamento(descricao) {
    var result = descricao.search(/Em processo de Tombamento/i);
    if(result !== -1){
        return {v:0, f:'Em processo de Tombamento'};
    }
    return  {v:1, f:'Tombado'}
}

